import numpy as np
import random
import matplotlib.pyplot as plt


class Node:
    def __init__(self, index):
        self.index = index
        self.children = []

    def add_child(self, child):
        self.children.append(child)


class Graph:
    def __init__(self, m, m0, alpha):
        self.nodes = []
        self.m = m
        self.alpha = alpha
        for i in range(m0):
            node = Node(i)
            for node0 in self.nodes:
                self.add_edge(node, node0)
            self.nodes.append(node)

    def add_edge(self, node1, node2):
        node1.add_child(node2)
        node2.add_child(node1)

    def add_node(self):
        index = len(self.nodes)
        new_node = Node(index)
        m1 = int(self.alpha * self.m)
        m_nodes = []
        while len(m_nodes) < m1:
            rand_index = random.randint(0, len(self.nodes) - 1)
            node = self.nodes[rand_index]
            if not m_nodes.__contains__(node):
                m_nodes.append(node)
        while len(m_nodes) < self.m:
            rand_index = random.randint(0, len(self.nodes) - 1)
            node = self.nodes[rand_index]
            if len(node.children) + len(m_nodes) < self.m:
                for child in node.children:
                    if not m_nodes.__contains__(child):
                        m_nodes.append(child)
            else:
                while len(m_nodes) < self.m:
                    random_index = random.randint(0, len(node.children) - 1)
                    child = node.children[random_index]
                    if not m_nodes.__contains__(child):
                        m_nodes.append(child)

        for node in m_nodes:
            self.add_edge(new_node, node)
        self.nodes.append(new_node)

    def calc_clustering_coefficient(self):
        # clustering coefficient = n / m
        n = 0
        m = 0
        for node in self.nodes:
            for node1 in node.children:
                for node2 in node.children:
                    if node1 == node2:
                        continue
                    m += 1
                    if node1.children.__contains__(node2):
                        n += 1
        if m == 0:
            return 1
        return n / m

    def calc_degree_distribution(self):
        dist = [0 for i in range(len(self.nodes))]
        for node in self.nodes:
            d = len(node.children)
            dist[d] = dist[d] + 1
        for i in range(1, len(self.nodes)):
            dist[i] = dist[i] + dist[i - 1]
        return (np.array(dist) + 1) / len(self.nodes)

    def calc_degree_density_distribution(self):
        dist = [0 for i in range(len(self.nodes))]
        for node in self.nodes:
            d = len(node.children)
            dist[d] = dist[d] + 1
        return np.array(dist) / len(self.nodes)

    def create_gephi_matrix(self):
        name = "Gephi_Data/edges__m{0}_alpha{1}.csv".format(self.m, self.alpha * 100)
        with open(name, "w") as writer:
            writer.write("")
        with open(name, "a") as writer:
            for node in self.nodes:
                for child in node.children:
                    writer.write("{0},{1}\n".format(node.index,child.index))


if __name__ == "__main__":
    n = 10000
    m0 = 30

    # change m
    alpha = 0.3
    cnt = 1
    for m in range(1, 20):
        graph = Graph(m, m0, alpha)
        for i in range(n):
            graph.add_node()
        degree = [i + 1 for i in range(len(graph.nodes))]
        cl = graph.calc_clustering_coefficient()
        degree_dis = graph.calc_degree_distribution()
        plt.figure(cnt)
        plt.ylabel('log(F)')
        plt.xlabel('log(Degree)')
        plt.title(
            "Degree distribution for Estimated Graph, m = {0}, alpha = {1}\n Clustering Coeff = {2}".format(
                m, alpha, cl))
        plt.loglog(degree, degree_dis, 'k')
        plt.savefig("Pic/Deg_dis_m{0}.png".format(m))
        plt.close(cnt)
        graph.create_gephi_matrix()
        cnt += 1

    #change alpha
    m = 10
    alpha = 0
    cnt += 1
    while alpha < 1:
        graph = Graph(m, m0, alpha)
        for i in range(n):
            graph.add_node()
        degree = [i + 1 for i in range(len(graph.nodes))]
        cl = graph.calc_clustering_coefficient()
        degree_dis = graph.calc_degree_distribution()
        plt.figure(cnt)
        plt.ylabel('log(F)')
        plt.xlabel('log(Degree)')
        plt.title(
            "Degree distribution for Estimated Graph, m = {0}, alpha = {1}\n Clustering Coeff = {2}".format(
                m, alpha, cl))
        plt.loglog(degree, degree_dis, 'k')
        plt.savefig("Pic/Deg_dis_alpha{0}.png".format(int(alpha * 100)))
        plt.close(cnt)
        graph.create_gephi_matrix()
        alpha += 0.05
        cnt += 1





# m = 5
# alpha = 0.1
# graph = Graph(m, m0, alpha)
# for _ in range(n):
#     graph.add_node()
# degree = [i for i in range(n + m0)]
#
# plt.figure(1)
# plt.subplot(211)
# plt.ylabel('F')
# plt.xlabel('Degree')
# plt.title("Degree distribution for Estimated Graph, m = {0}".format(m))
# # plt.loglog(degree, est_distribution, 'k')
# plt.loglog(degree, est_density_distribution, 'k')
#
# plt.subplot(212)
# plt.ylabel('F')
# plt.xlabel('Degree')
# plt.title("Degree distribution for Simulated Graph, m = {0}".format(m))
# # plt.loglog(degree, sim_distribution, 'k')
# plt.loglog(degree, sim_density_distribution, 'k')
# plt.show()

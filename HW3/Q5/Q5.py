import numpy as np
import random
import matplotlib.pyplot as plt

plt.switch_backend('agg')


# l = ( log ((n-1)(<d> - 1)/<d> + 1 ) ) / ( log ( <d> ) )


class Node:
    def __init__(self, value):
        self.value = value
        self.children = []
        self.visited = False
        self.depth = 0

    def add_child(self, new_child):
        self.children.append(new_child)


class Graph:
    def __init__(self):
        self.nodes = []

    def add_node(self, new_node):
        self.nodes.append(new_node)

    def add_edge(self, node1, node2):
        node1.add_child(node2)
        node2.add_child(node1)

    def reset(self):
        for node in self.nodes:
            node.visited = False
            node.depth = 0

    def reset_edges(self):
        for node in self.nodes:
            node.children = []

    def bfs_diameter(self, root):
        self.reset()
        root.visited = True
        root.depth = 0
        expanding_nodes = []
        expanding_nodes.append(root)
        max_depth = 0
        while len(expanding_nodes) > 0:
            node = expanding_nodes[0]
            expanding_nodes.pop(0)
            for child in node.children:
                if not child.visited:
                    child.visited = True
                    child.depth = node.depth + 1
                    expanding_nodes.append(child)
                    if child.depth > max_depth:
                        max_depth = child.depth
        return max_depth

    def all_visited(self):
        for node in self.nodes:
            if node.visited == False:
                return False
        return True

    def calc_diameter(self):
        diameter = None
        for node in self.nodes:
            d = self.bfs_diameter(node)
            if ((diameter != None and d > diameter) or diameter == None) and self.all_visited():
                diameter = d
        return diameter


def create_random_graph(graph, p):
    graph.reset()
    graph.reset_edges()
    for node1 in graph.nodes:
        for node2 in graph.nodes:
            if node1.value < node2.value and random.random() <= p:
                graph.add_edge(node1, node2)


def diameter_of_random_graph(graph, p):
    create_random_graph(graph, p)
    return graph.calc_diameter()


def calc_estimated_diameter(n, p):
    estimated_deg = (n - 1) * p
    if estimated_deg <= 1:
        return 0
    s1 = (n - 1) * (estimated_deg - 1) / estimated_deg + 1
    return np.log10(s1) / np.log10(estimated_deg)


def a():
    n_max = 2000
    n_min = 1000
    n_step = 10
    p = 1 / 100
    est_diameter = []
    sim_diameter = []
    n = n_min
    n_list = []
    while n <= n_max:
        graph = Graph()
        for i in range(n):
            graph.add_node(Node(i))
        n_list.append(n)
        d = calc_estimated_diameter(n, p)
        est_diameter.append(d)
        d = None
        while d == None:
            d = diameter_of_random_graph(graph, p)
        sim_diameter.append(d)
        print(n)
        n += n_step

    plt.figure(1, figsize=(18, 12))
    plt.subplot(211)
    plt.ylabel('Diameter')
    plt.xlabel('n')
    plt.title("Diameter vs n for Estimated Graph")
    plt.plot(n_list, est_diameter, 'k')

    plt.subplot(212)
    plt.ylabel('Diameter')
    plt.xlabel('n')
    plt.title("Diameter vs n for Simulated Graph")
    plt.plot(n_list, sim_diameter, 'k')
    plt.show()
    plt.savefig('a.png', dpi=100)


def b():
    n = 1000
    p_min = 1 / 100
    p_max = 1
    p_step = 5 / 1000
    est_diameter = []
    sim_diameter = []
    p = p_min
    p_list = []
    graph = Graph()
    for i in range(n):
        graph.add_node(Node(i))
    while p <= p_max:
        p_list.append(p)
        d = calc_estimated_diameter(n, p)
        est_diameter.append(d)
        d = None
        while d == None:
            d = diameter_of_random_graph(graph, p)
        sim_diameter.append(d)
        print(p)
        p += p_step

    plt.figure(2, figsize=(18, 12))
    plt.subplot(211)
    plt.ylabel('Diameter')
    plt.xlabel('p')
    plt.title("Diameter vs p for Estimated Graph")
    plt.plot(p_list, est_diameter, 'k')

    plt.subplot(212)
    plt.ylabel('Diameter')
    plt.xlabel('p')
    plt.title("Diameter vs p for Simulated Graph")
    plt.plot(p_list, sim_diameter, 'k')
    plt.show()
    plt.savefig('b.png', dpi=100)


if __name__ == "__main__":
    # a()
    # b()

    n = 100
    p_min = 1 / 100
    p_max = 1
    p_step = 5 / 1000
    est_diameter = []
    sim_diameter = []
    p = 1
    p_list = []
    graph = Graph()
    for i in range(n):
        graph.add_node(Node(i))
    d = None
    while d == None:
        d = diameter_of_random_graph(graph, p)
    print(d)
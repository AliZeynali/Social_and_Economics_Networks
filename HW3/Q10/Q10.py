import random
import math
import numpy as np
import matplotlib.pyplot as plt


class Node:
    def __init__(self):
        self.children = []

    def add_child(self, child):
        self.children.append(child)


class Graph:
    def __init__(self, m, m0):
        self.nodes = []
        self.m = m
        for i in range(m0):
            node = Node()
            for node0 in self.nodes:
                self.add_edge(node, node0)
            self.nodes.append(node)

    def add_edge(self, node1, node2):
        node1.add_child(node2)
        node2.add_child(node1)
    def create_random_index_with_degree(self):
        indexes = []
        for index in range(len(self.nodes)):
            for i in range(len(self.nodes[index].children)):
                indexes.append(index)
        return indexes


    def add_node(self):
        new_node = Node()
        random_m_nodes = []
        while (len(random_m_nodes) < self.m):
            random_indexes = self.create_random_index_with_degree()
            rand_node = random.randint(0, len(random_indexes) - 1)
            rand_index = random_indexes[rand_node]
            if not random_m_nodes.__contains__(self.nodes[rand_index]):
                random_m_nodes.append(self.nodes[rand_index])
            # random_m_nodes.append(self.nodes[rand_index])
        for node in random_m_nodes:
            self.add_edge(new_node, node)
        self.nodes.append(new_node)

    def calc_degree_distribution(self):
        dist = [0 for i in range(len(self.nodes))]
        for node in self.nodes:
            d = len(node.children)
            dist[d] = dist[d] + 1
        for i in range(1, len(self.nodes)):
            dist[i] = dist[i] + dist[i - 1]
        return np.array(dist) / len(self.nodes)

    def calc_degree_density_distribution(self):
        dist = [0 for i in range(len(self.nodes))]
        for node in self.nodes:
            d = len(node.children)
            dist[d] = dist[d] + 1
        return np.array(dist) / len(self.nodes)


def calc_estimation_distribution(n, m):
    #
    #               - (d - m)
    #               -----------
    #                   m
    # F (d) = 1 - e
    #
    dist = [0 for i in range(n)]
    for d in range(n):
        dist[d] = 1 - math.exp(-(d - m) / m)
    return dist

def calc_estimation_density_distribution(n,m):
    dist = calc_estimation_distribution(n,m)
    i = len(dist) -1
    while (i > 0):
        dist[i] = dist[i] - dist[i-1]
        i -= 1
    return dist


if __name__ == "__main__":
    n = 500
    m0 = 5
    m = 1
    # m = 5
    graph = Graph(m, m0)
    for _ in range(n):
        graph.add_node()
    # est_distribution = calc_estimation_distribution(n + m0, m)
    # sim_distribution = graph.calc_degree_distribution()
    est_density_distribution = calc_estimation_density_distribution(n + m0, m)
    sim_density_distribution = graph.calc_degree_density_distribution()
    degree = [i for i in range(n + m0)]

    plt.figure(1)
    plt.subplot(211)
    plt.ylabel('F')
    plt.xlabel('Degree')
    plt.title("Degree distribution for Estimated Graph, m = {0}".format(m))
    # plt.loglog(degree, est_distribution, 'k')
    plt.loglog(degree, est_density_distribution, 'k')

    plt.subplot(212)
    plt.ylabel('F')
    plt.xlabel('Degree')
    plt.title("Degree distribution for Simulated Graph, m = {0}".format(m))
    # plt.loglog(degree, sim_distribution, 'k')
    plt.loglog(degree, sim_density_distribution, 'k')
    plt.show()
    # plt.savefig('Q10.png')

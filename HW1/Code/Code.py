import matplotlib.pyplot as plt
import time
import math
import numpy as np


class Node:
    def __init__(self, number):
        self.number = number
        self.children = []
        self.visited = False
        self.distance = 0

    def add_child(self, child):
        self.children.append(child)


class Graph:
    def __init__(self, number):
        self.nodes = []
        self.number = number

    def get_degrees(self):
        degrees = []
        max = 0
        for node in self.nodes:
            degrees.append(len(node.children))
            if max < len(node.children):
                max = len(node.children)
        return degrees, max

    def find_node(self, node_number):
        for node in self.nodes:
            if node.number == node_number:
                return node
        return None

    def path_length(self, start_node):
        for node in self.nodes:
            node.visited = False
            node.distance = 0
        answer = 0
        cnt = 0
        frontier = [start_node]
        start_node.distance = 0
        start_node.visited = True
        while (len(frontier) > 0):
            for child in frontier[0].children:
                if child.visited == False:
                    frontier.append(child)
                    child.distance = frontier[0].distance + 1
                    child.visited = True
                    answer += child.distance
                    cnt += 1
            frontier.pop(0)
        return answer, cnt

    def clustering_coefficient(self, my_node):
        length = len(my_node.children)
        if length <= 1:
            return 0
        triangles = 0
        for node in my_node.children:
            if node == None:
                print(my_node.children)
            for node2 in my_node.children:
                if node == node2:
                    continue
                # print(node)
                # print(node.children)
                if node.children.__contains__(node2):
                    triangles += 1
        return triangles / (length * (length - 1))

    def average_clustering_coefficient(self):
        answer = 0
        for node in self.nodes:
            answer += self.clustering_coefficient(node)
        return answer / len(self.nodes)

    def average_degree(self):
        answer = 0
        for node in self.nodes:
            answer += len(node.children)
        return answer / len(self.nodes)

    def average_path_length(self):
        answer = 0
        cnt = 0
        for node in self.nodes:
            paths, numbers = self.path_length(node)
            # print("In Graph {0}, Path length calculated for node {1}".format(self.number, node.number))
            answer += paths
            cnt += numbers
        return answer / cnt

    def estimate_average_path_length(self):
        answer = 0
        cnt = 0
        random_index = np.random.random_integers(0, len(self.nodes) - 1, 500)
        for index in random_index:
            node = self.nodes[index]
            paths, numbers = self.path_length(node)
            # print("In Graph {0}, Path length calculated for node {1}".format(self.number, node.number))
            answer += paths
            cnt += numbers
        return answer / cnt


def print_star():
    print("\n" + 10 * '*' + "\n")


def plot_histogram(data1, data2, data3, max1=20, max2=20, max3=20):
    array1 = [0 for i in range(max1 + 1)]
    for x in data1:
        if x > max1:
            continue
        array1[x] = array1[x] + 1

    array2 = [0 for i in range(max2 + 1)]
    for x in data2:
        if x > max2:
            continue
        array2[x] = array2[x] + 1

    array3 = [0 for i in range(max3 + 1)]
    for x in data3:
        if x > max3:
            continue
        array3[x] = array3[x] + 1

    fig1 = plt.figure(1)
    plt.subplot(311)
    plt.plot([i for i in range(1, max1 + 1)], array1[1:], marker='o')
    plt.ylabel('Dataset1')

    plt.subplot(312)
    plt.plot([i for i in range(1, max2 + 1)], array2[1:], marker='o')
    plt.ylabel('Dataset2')

    plt.subplot(313)
    plt.plot([i for i in range(1, max3 + 1)], array3[1:], marker='o')
    plt.ylabel('Dataset3')
    # plt.show()
    fig1.savefig("Histograms.png")
    plot_log_log_histogram(array1[1:], array2[1:], array3[1:], max1, max2, max3)


def plot_log_log_histogram(data1, data2, data3, max1, max2, max3):
    fig1 = plt.figure(2)
    plt.subplot(311)
    data1 = np.log10(np.array(data1) + 1)
    plt.plot(np.log10([i for i in range(1, max1 + 1)]), data1, marker='o')
    plt.ylabel('Log_Log : Dataset1')

    plt.subplot(312)
    data2 = np.log10(np.array(data2) + 1)
    plt.plot(np.log10([i for i in range(1, max2 + 1)]), data2, marker='o')
    plt.ylabel('Log_Log : Dataset2')

    plt.subplot(313)
    data3 = np.log10(np.array(data3) + 1)
    plt.plot(np.log10([i for i in range(1, max3 + 1)]), data3, marker='o')
    plt.ylabel('Log_Log : Dataset3')
    # plt.show()
    fig1.savefig("Log_Log_Histograms.png")


if __name__ == "__main__":
    dataset1 = []
    dataset2 = []
    dataset3 = []
    reader1 = open("../Datasets/dataset1.txt")
    reader2 = open("../Datasets/dataset2.txt")
    reader3 = open("../Datasets/dataset3.txt")
    for line in reader1:
        data = line.replace("\n", "").split("\t")
        dataset1.append([int(data[0]), int(data[1])])
    for line in reader2:
        data = line.replace("\n", "").split("\t")
        dataset2.append([int(data[0]), int(data[1])])
    for line in reader3:
        data = line.replace("\n", "").split("\t")
        dataset3.append([int(data[0]), int(data[1])])

    reader1.close()
    reader2.close()
    reader3.close()
    writer = open("Informations.txt", "w")

    print("Length of dataset1 is : " + str(len(dataset1)))
    print("Length of dataset2 is : " + str(len(dataset2)))
    print("Length of dataset3 is : " + str(len(dataset3)))
    print_star()
    current_time = time.time()
    graph1 = Graph(1)
    graph2 = Graph(2)
    graph3 = Graph(3)

    node_number = dataset1[0][0] - 1
    max1 = 0
    for i in range(len(dataset1)):
        if max1 < dataset1[i][0]:
            max1 = dataset1[i][0]
        if max1 < dataset1[i][1]:
            max1 = dataset1[i][1]
            # if node_number < dataset1[i][0]:
            #     new_node = Node(dataset1[i][0])
            #     graph1.nodes.append(new_node)
            # node_number = dataset1[i][0]

    node_number = dataset2[0][0] - 1
    max2 = 0
    for i in range(len(dataset2)):
        if max2 < dataset2[i][0]:
            max2 = dataset2[i][0]
        if max2 < dataset2[i][1]:
            max2 = dataset2[i][1]
            # if node_number < dataset2[i][0]:
            #     new_node = Node(dataset2[i][0])
            #     graph2.nodes.append(new_node)
            # node_number = dataset2[i][0]

    node_number = dataset3[0][0] - 1
    max3 = 0
    for i in range(len(dataset3)):
        if max3 < dataset3[i][0]:
            max3 = dataset3[i][0]
        if max3 < dataset3[i][1]:
            max3 = dataset3[i][1]
            # if node_number < dataset3[i][0]:
            #     new_node = Node(dataset3[i][0])
            #     graph3.nodes.append(new_node)
            # node_number = dataset3[i][0]

    for i in range(max1 + 1):
        new_node = Node(i)
        graph1.nodes.append(new_node)
    for i in range(max2 + 1):
        new_node = Node(i)
        graph2.nodes.append(new_node)
    for i in range(max3 + 1):
        new_node = Node(i)
        graph3.nodes.append(new_node)

    for i in range(len(dataset1)):
        first_node = graph1.find_node(dataset1[i][0])
        second_node = graph1.find_node(dataset1[i][1])
        if second_node == None:
            new_node = Node(dataset1[i][1])
            second_node = new_node
            graph1.nodes.append(second_node)
        first_node.add_child(second_node)
        second_node.add_child(first_node)
    print("Graph number  1 created in {0:.1f} seconds".format(time.time() - current_time))
    print("Graph number  1 has {0} nodes\n".format(len(graph1.nodes)))
    current_time = time.time()

    for i in range(len(dataset2)):
        first_node = graph2.find_node(dataset2[i][0])
        second_node = graph2.find_node(dataset2[i][1])
        if second_node == None:
            new_node = Node(dataset2[i][1])
            second_node = new_node
            graph2.nodes.append(second_node)
        first_node.add_child(second_node)
        second_node.add_child(first_node)
    print("Graph number  2 created in {0:.1f} seconds".format(time.time() - current_time))
    print("Graph number  2 has {0} nodes\n".format(len(graph2.nodes)))
    current_time = time.time()

    for i in range(len(dataset3)):
        first_node = graph3.find_node(dataset3[i][0])
        second_node = graph3.find_node(dataset3[i][1])
        if second_node == None:
            new_node = Node(dataset3[i][1])
            second_node = new_node
            graph3.nodes.append(second_node)
        first_node.add_child(second_node)
        second_node.add_child(first_node)
    print("Graph number  3 created in {0:.1f} seconds".format(time.time() - current_time))
    print("Graph number  3 has {0} nodes".format(len(graph3.nodes)))
    print_star()
    current_time = time.time()

    degrees1, max_deg1 = graph1.get_degrees()
    degrees2, max_deg2 = graph2.get_degrees()
    degrees3, max_deg3 = graph3.get_degrees()

    plot_histogram(degrees1, degrees2, degrees3)
    print("Plot done in {0:.1f} seconds".format(time.time() - current_time))
    current_time = time.time()

    average_degree1 = graph1.average_degree()
    writer.write("Average degree for dataset1 is:\t{0:.2f}\n".format(average_degree1))
    print("Average degree for dataset1 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_degree1,
                                                                                      time.time() - current_time))

    current_time = time.time()
    average_degree2 = graph2.average_degree()
    writer.write("Average degree for dataset2 is:\t{0:.2f}\n".format(average_degree2))
    print("Average degree for dataset2 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_degree2,
                                                                                      time.time() - current_time))
    current_time = time.time()

    average_degree3 = graph3.average_degree()
    writer.write("Average degree for dataset3 is:\t{0:.2f}\n\n".format(average_degree3))
    print("Average degree for dataset3 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_degree3,
                                                                                      time.time() - current_time))
    current_time = time.time()
    print_star()

    average_clustering_coeffincient1 = graph1.average_clustering_coefficient()
    writer.write("Average Clustering Coefficient for dataset1 is:\t{0:.2f}\n".format(average_clustering_coeffincient1))
    print("Average Clustering Coefficient for dataset1 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(
        average_clustering_coeffincient1, time.time() - current_time))
    current_time = time.time()

    average_clustering_coeffincient2 = graph2.average_clustering_coefficient()
    writer.write("Average Clustering Coefficient for dataset2 is:\t{0:.2f}\n".format(average_clustering_coeffincient2))
    print("Average Clustering Coefficient for dataset2 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(
        average_clustering_coeffincient2, time.time() - current_time))
    current_time = time.time()

    average_clustering_coeffincient3 = graph3.average_clustering_coefficient()
    writer.write(
        "Average Clustering Coefficient for dataset3 is:\t{0:.2f}\n\n".format(average_clustering_coeffincient3))
    print("Average Clustering Coefficient for dataset3 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(
        average_clustering_coeffincient3, time.time() - current_time))
    current_time = time.time()
    print_star()

    average_path_length1 = graph1.average_path_length()
    writer.write("Average Path length for dataset1 is:\t{0:.2f}\n".format(average_path_length1))
    print("Average Path length for dataset1 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_path_length1,
                                                                                           time.time() - current_time))
    current_time = time.time()

    average_path_length2 = graph2.average_path_length()
    writer.write("Average Path length for dataset2 is:\t{0:.2f}\n".format(average_path_length2))
    print("Average Path length for dataset2 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_path_length2,
                                                                                           time.time() - current_time))
    current_time = time.time()

    average_path_length3 = graph3.average_path_length()
    writer.write("Average Path length for dataset3 is:\t{0:.2f}\n\n".format(average_path_length3))
    print("Average Path length for dataset3 is:\t{0:.2f}\t,calculate time: {1:.1f}".format(average_path_length3,
                                                                                           time.time() - current_time))
    print_star()
    writer.close()

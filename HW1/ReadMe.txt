Two datasets had analyzed.

Result of small dataset are in "Little Dataset" folder
Result of large dataset are in "Large Dataset" folder

Each Dataset has 4 files:

1- Histograms.png:

	All three histograms which related to each dataset

2- Log_Log_Histograms.png:

	All three log-log histograms which related to each dataset

3- Informations.txt:

	Informations of each graph

4- log.txt:

	Log file of executed python file
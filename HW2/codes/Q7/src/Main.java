import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        File file = new File("./Signed_Network.txt");
        ArrayList<Node> nodes = new ArrayList<>();
        ArrayList<Integer> nodeNumbers = new ArrayList<>();
        int posNumber = 0;
        int negNumber = 0;
        double p;
        try {
            Scanner scanner = new Scanner(file);
            String header = scanner.nextLine();
            while (scanner.hasNextLine()) {
                int a = scanner.nextInt();
                int b = scanner.nextInt();
                int edge = scanner.nextInt();
                if (edge == 1){
                    posNumber++;
                }else if(edge == -1){
                    negNumber++;
                }
                Node aNode = null;
                Node bNode = null;
                if (!nodeNumbers.contains(a)) {
                    aNode = new Node(a);
                    nodes.add(aNode);
                    nodeNumbers.add(a);
                } else {
                    for (Node node : nodes) {
                        if (node.getNumber() == a) {
                            aNode = node;
                            break;
                        }
                    }
                }
                if (!nodeNumbers.contains(b)) {
                    bNode = new Node(b);
                    nodes.add(bNode);
                    nodeNumbers.add(b);
                } else {
                    for (Node node : nodes) {
                        if (node.getNumber() == b) {
                            bNode = node;
                            break;
                        }
                    }
                }
                aNode.addEdge(bNode, edge);
                if (a != b) {
                    bNode.addEdge(aNode, edge);
                }


            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        p = (posNumber) / (double)(posNumber + negNumber);
        System.out.println("Graph created");
        int[] triangleValues = triangleValues(nodes);
        System.out.println("t0 in raw graph is :\t" + Integer.toString(triangleValues[0]));
        System.out.println("t1 in raw graph is :\t" + Integer.toString(triangleValues[1]));
        System.out.println("t2 in raw graph is :\t" + Integer.toString(triangleValues[2]));
        System.out.println("t3 in raw graph is :\t" + Integer.toString(triangleValues[3]));
        System.out.println("");
        System.out.println("p is :\t"+ Double.toString(p));
        changeEdgeValuesBypPercentage(nodes,p);
        int[] newTriangleValues = triangleValues(nodes);
        System.out.println("t0 in changed graph is :\t" + Integer.toString(newTriangleValues[0]));
        System.out.println("t1 in changed graph is :\t" + Integer.toString(newTriangleValues[1]));
        System.out.println("t2 in changed graph is :\t" + Integer.toString(newTriangleValues[2]));
        System.out.println("t3 in changed graph is :\t" + Integer.toString(newTriangleValues[3]));


    }

    public static int[] triangleValues(ArrayList<Node> nodes) {
        int[] answer = new int[4];
        int triangleSum = 0;
        for (Node firstNode : nodes) {
            for (Edge firstEdge : firstNode.getEdges()) {
                Node secondNode = firstEdge.getDestNode();
                if (secondNode.getNumber() <= firstNode.getNumber()){
                    continue;
                }
                for (Edge secondEdge: secondNode.getEdges()){
                    Node thirdNode = secondEdge.getDestNode();
                    if (thirdNode.getNumber() <= firstNode.getNumber() || thirdNode.getNumber() <= secondNode.getNumber() ){
                        continue;
                    }
                    for (Edge thirdEdge : thirdNode.getEdges()){
                        if (thirdEdge.getDestNode() == firstNode){
                            triangleSum = firstEdge.getValue() + secondEdge.getValue() + thirdEdge.getValue();
                            if (triangleSum == -3){
                                answer[0] = answer[0] + 1;
                            }else if(triangleSum == -1){
                                answer[1] = answer[1] + 1;
                            }else if(triangleSum == 1){
                                answer[2] = answer[2] + 1;
                            }else if(triangleSum == 3){
                                answer[3] = answer[3] + 1;
                            }
                            break;
                        }
                    }
                }
            }
        }
        return answer;
    }
    public static void changeEdgeValuesBypPercentage(ArrayList<Node> nodes, double p){
        Random random = new Random();
        double randomNumber;
        for (Node firstNode: nodes){
            for (Edge edge1 : firstNode.getEdges()){
                Node secondNode = edge1.getDestNode();
                if (secondNode.getNumber() <= firstNode.getNumber()){
                    continue;
                }
                randomNumber = random.nextDouble();
                if (randomNumber <= p){
                    edge1.setValue(1);
                }else {
                    edge1.setValue(-1);
                }
                for (Edge edge2: secondNode.getEdges()){
                    if (edge2.getDestNode() == firstNode){
                        if (randomNumber <= p){
                            edge2.setValue(1);
                        }else {
                            edge2.setValue(-1);
                        }
                        break;
                    }
                }
            }
        }
    }
}

class Node {
    private int number;
    private ArrayList<Edge> edges = new ArrayList<>();

    public Node(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public void addEdge(Node node, int value) {
        this.edges.add(new Edge(node, value));
    }
}

class Edge {
    private Node destNode;
    private int value;

    public Edge(Node destNode, int value) {
        this.destNode = destNode;
        this.value = value;
    }

    public Node getDestNode() {
        return destNode;
    }

    public void setDestNode(Node destNode) {
        this.destNode = destNode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

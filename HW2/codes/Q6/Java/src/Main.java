import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {
        int[][] table = new int[100][100];
        ArrayList<Integer> values = new ArrayList<>();
        ArrayList<Integer> schelling_values = new ArrayList<>();
        Random random = new Random();
        int blue_percentage = 48;
        int red_percentage = 48 + 48;
        int n = 100;
        int m = 100;
        int numberOfRepeat = 1000;
        int cnt;
        for (int k = 0; k < numberOfRepeat; k++) {
            cnt = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    int random_number = random.nextInt(100);
                    if (random_number < blue_percentage) {
                        table[i][j] = 1;
                    } else if (random_number < red_percentage) {
                        table[i][j] = 2;
                    } else {
                        table[i][j] = 0;
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (checkBorders(i, j, table)) {
                        cnt++;
                    }
                }
            }
            values.add(cnt);
            table = schelling_alg(table);
            cnt = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (checkBorders(i, j, table)) {
                        cnt++;
                    }
                }
            }
            schelling_values.add(cnt);



        }
        String fileName = "values_" + Integer.toString(numberOfRepeat) + ".txt";
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        for (int value : values) {
            writer.write(value + "\n");

        }
        writer.close();

        fileName = "schelling_values_" + Integer.toString(numberOfRepeat) + ".txt";
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(fileName));
        for (int value : schelling_values) {
            writer2.write(value + "\n");

        }
        writer2.close();

    }

    public static boolean checkBorders(int i, int j, int[][] table) {
        int n = table.length;
        int m = table[0].length;
        int color = table[i][j];
        if (color == 0) {
            return false;
        }
        int up_i = i - 1;
        if (up_i < 0) {
            up_i += n;
        }
        int down_i = i + 1;
        if (down_i >= n) {
            down_i -= n;
        }
        int left_j = j - 1;
        if (left_j < 0) {
            left_j += m;
        }
        int right_j = j + 1;
        if (right_j >= m) {
            right_j -= m;
        }

        //Up left
        if (table[up_i][left_j] != color) {
            return false;
        }
        //Up
        if (table[up_i][j] != color) {
            return false;
        }
        //Up right
        if (table[up_i][right_j] != color) {
            return false;
        }
        //left
        if (table[i][left_j] != color) {
            return false;
        }
        //right
        if (table[i][right_j] != color) {
            return false;
        }
        //down left
        if (table[down_i][left_j] != color) {
            return false;
        }
        //down
        if (table[down_i][j] != color) {
            return false;
        }
        //down right
        if (table[down_i][right_j] != color) {
            return false;
        }
        return true;

    }

    public static boolean schelling_border_check(int i, int j, int[][] table) {
        int t = 3;
        int n = table.length;
        int m = table[0].length;
        int color = table[i][j];
        if (color == 0) {
            return true;
        }
        int up_i = i - 1;
        if (up_i < 0) {
            up_i += n;
        }
        int down_i = i + 1;
        if (down_i >= n) {
            down_i -= n;
        }
        int left_j = j - 1;
        if (left_j < 0) {
            left_j += m;
        }
        int right_j = j + 1;
        if (right_j >= m) {
            right_j -= m;
        }
        int cnt = 0;
        //Up left
        if (table[up_i][left_j] == color) {
            cnt++;
        }
        //Up
        if (table[up_i][j] == color) {
            cnt++;
        }
        //Up right
        if (table[up_i][right_j] == color) {
            cnt++;
        }
        //left
        if (table[i][left_j] == color) {
            cnt++;
        }
        //right
        if (table[i][right_j] == color) {
            cnt++;
        }
        //down left
        if (table[down_i][left_j] == color) {
            cnt++;
        }
        //down
        if (table[down_i][j] == color) {
            cnt++;
        }
        //down right
        if (table[down_i][right_j] == color) {
            cnt++;
        }
        if (cnt < t) {
            return false;
        }
        return true;


    }

    public static int[][] schelling_alg(int[][] table) {
        int n = table.length;
        int m = table[0].length;
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (!schelling_border_check(i, j, table)) {
                    while (true){
                        int x = random.nextInt(m);
                        int y = random.nextInt(n);
                        if (table[y][x] == 0){
                            table[y][x] = table[i][j];
                            table[i][j] = 0;
                            break;
                        }

                    }
                    return schelling_alg(table);


                }
            }
        }
        return table;

    }
}

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

values = []
n = 1000
with open("../values_{0}.txt".format(n)) as reader:
    for _ in range(100):
        values.append(int(reader.readline()))

beans = []
x = 20
while(x <= 35):
    beans.append(x+0.5)
    x += 1
plt.hist(values, beans)
plt.ylabel('Number of 9 Squares')
plt.title("Histogram of raw tables for " + str(n) + " repeat test.")
plt.show()

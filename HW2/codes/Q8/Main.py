import networkx as nx
import pylab as plt

# import igraph

inf = 1000


class Node:
    def __init__(self, number):
        self.number = number
        self.following = []
        self.betweenness = 0
        self.shortest_paths = []
        self.parents = []
        self.depth = inf
        self.number_of_shortest_paths_cross_this_node = 0
        self.number_of_shortest_paths_which_i_am_at_last = 0
        self.number_of_shortest_paths_which_i_am_at_start = 0
        self.visited = False

    def follow(self, node):
        self.following.append(node)

    def reset(self):
        self.shortest_paths = []
        self.parents = []
        self.depth = inf
        self.visited = False

    def has_connection(self, node):
        for node_x in self.following:
            if node_x == node:
                return True
        return False

    def add_crossing_path(self):
        self.number_of_shortest_paths_cross_this_node += 1

    def add_end_path(self):
        self.number_of_shortest_paths_which_i_am_at_last += 1


def BFS(starting_node):
    starting_node.shortest_paths = []
    bfs_nodes = []
    bfs_nodes.append(starting_node)
    starting_node.depth = 1
    while (len(bfs_nodes) > 0):
        node = bfs_nodes[0]
        if len(node.parents) == 0:
            node.shortest_paths = [[node.number]]
        else:
            for parent in node.parents:
                for path in parent.shortest_paths:
                    path2 = path.copy()
                    path2.append(node.number)
                    node.shortest_paths.append(path2)
                    # node.add_end_path()
                    # if node.depth > 2:
                    #     parent.add_crossing_path()

        for child in node.following:
            if child.depth > node.depth:
                if child.depth > node.depth + 1:
                    child.depth = node.depth + 1
                    bfs_nodes.append(child)
                    child.visited = True
                child.parents.append(node)
        bfs_nodes.pop(0)


def simple_BFS(starting_node):
    bfs_queue = []
    bfs_queue.append(starting_node)
    starting_node.visited = True
    while (len(bfs_queue) > 0):
        node = bfs_queue[0]
        for child in node.following:
            if child.visited == False:
                child.visited = True
                bfs_queue.append(child)
        bfs_queue.pop(0)


def highest_betweenness_edge(nodes):
    betweenness_matrix = [[0 for _ in range(len(nodes))] for __ in range(len(nodes))]
    for node in nodes:
        for node2 in nodes:
            node2.reset()
        BFS(node)
        for node3 in nodes:
            for path in node3.shortest_paths:
                for index in range(len(path) - 1):
                    betweenness_matrix[path[index]][path[index + 1]] = betweenness_matrix[path[index]][
                                                                           path[index + 1]] + 1
    max_betweenness = 0
    a = 0
    b = 0
    sum = 0
    for i in range(len(betweenness_matrix)):
        for j in range(len(betweenness_matrix[0])):
            sum += betweenness_matrix[i][j]
            if betweenness_matrix[i][j] > max_betweenness:
                max_betweenness = betweenness_matrix[i][j]
                a = i
                b = j
    with open("Betweenness.txt", "a") as writer:
        for i in range(len(betweenness_matrix)):
            for j in range(len(betweenness_matrix[0])):
                if betweenness_matrix[i][j] > 0 and i < j:
                    writer.write("Betweenness for edge between node: {0} and node: {1} is:\t{2}".format(i, j,
                                                                                                        betweenness_matrix[
                                                                                                            i][
                                                                                                            j] / sum))
                    if i == a and j == b:
                        writer.write("\t <--- max betweenness edge")
                    writer.write("\n")
        writer.write("\n" + 20 * '*' + "\n\n")
    print("Number of shortest path on the maximum edge was:\t{0}".format(max_betweenness))
    return [nodes[a], nodes[b]]


def triadic_closure(nodes):
    strong_edges = [[0 for _ in range(len(nodes))] for __ in range(len(nodes))]
    for node1 in nodes:
        for node2 in node1.following:
            if node1.number > node2.number:
                continue
            flag = False
            cnt = 0
            for node3 in node1.following:
                for node4 in node2.following:
                    if node3 == node4:
                        cnt += 1
                        if cnt > 1:
                            flag = True
                            strong_edges[node1.number][node2.number] = 1
                    if (flag == True):
                        break
                if (flag == True):
                    break
    cnt = 0
    with open("triadic_closure.txt", "a") as writer:
        for node1 in nodes:
            for node2 in node1.following:
                if node1.number > node2.number:
                    continue
                if strong_edges[node1.number][node2.number] == 1:
                    writer.write("Edge between node: {0} and {1} is\tStrong\n".format(node1.number, node2.number))
                else:
                    writer.write("Edge between node: {0} and {1} is\tWeak\n".format(node1.number, node2.number))

        for node1 in nodes:
            for node2 in node1.following:
                if node1.number > node2.number:
                    continue
                for node3 in node1.following:
                    if node3.has_connection(node2):
                        continue
                    edge1_i = node1.number
                    edge1_j = node2.number
                    edge2_i = node1.number if node1.number < node3.number else node3.number
                    edge2_j = node3.number if node1.number < node3.number else node1.number
                    if (strong_edges[edge1_i][edge1_j] == 1 and strong_edges[edge2_i][edge2_j] == 1):
                        cnt += 1


        if cnt == 0:
            writer.write("\n\nGraph passed Strong triadic closure test successfully.\n\n\n\n\n\n")
        else:
            writer.write("\n\nStrong triadic closure test failed for {0} times.\n\n\n\n\n\n".format(cnt))

    return strong_edges


def mapping(s):
    return s


if __name__ == "__main__":
    all_nodes = []
    number_of_nodes = 60
    number_of_edges = 0
    for i in range(0, number_of_nodes):
        all_nodes.append(Node(i))

    with open("net.txt") as reader:
        while (True):
            string = reader.readline().replace("\n", "")
            if len(string) == 0:
                break
            node_number = int(string)
            node = all_nodes[node_number]
            while (True):
                following_node_number = reader.readline().replace("\n", "")
                if following_node_number == "#":
                    break
                following_node_number = int(following_node_number)
                following_node = all_nodes[following_node_number]
                if node.has_connection(following_node):
                    continue
                node.follow(following_node)
                following_node.follow(node)
                number_of_edges += 1
    print("Graph created")

    g = nx.Graph()
    for i in range(1, 59):
        g.add_node(str(i))
    edges = []
    for node in all_nodes:
        node_name = str(node.number)
        for following in node.following:
            edge = node_name + str(following.number)
            edges.append(edge)
            g.add_edge(node_name, str(following.number))
    #
    nx.relabel_nodes(g, mapping, copy=False)
    # nx.draw(g, with_labels=True)
    # plt.savefig("network.png")  # save as png
    # plt.show()  # display
    number_of_communities = 1
    while (number_of_communities < 3):
        print("Number of communities:\t{0}".format(number_of_communities))
        pair_nodes = highest_betweenness_edge(all_nodes)
        node1 = pair_nodes[0]
        node2 = pair_nodes[1]
        print("Highest betweenness edge was:\t( {0} , {1} ) --> Deleted ".format(node1.number, node2.number))
        node1.following.remove(node2)
        node2.following.remove(node1)
        for node in all_nodes:
            node.reset()
        number_of_communities = 0
        for node in all_nodes:
            if (node.visited == False):
                simple_BFS(node)
                number_of_communities += 1
        triadic_closure(all_nodes)

    g = nx.Graph()
    for i in range(1, 59):
        g.add_node(str(i))
    edges = []
    for node in all_nodes:
        node_name = str(node.number)
        for following in node.following:
            edge = node_name + str(following.number)
            edges.append(edge)
            g.add_edge(node_name, str(following.number))
            #
            # nx.draw(g, with_labels=True)
            # plt.savefig("network_communities.png")  # save as png
            # plt.show()  # display

            # g = igraph.Graph()
            # g.add_vertices(59)
            # g.vs["name"] = [str(x) for x in range(1, 60)]
            # edges = []
            # for node in all_nodes:
            #     node_name = str(node.number)
            #     for following in node.following:
            #         edge = (node_name, str(following.number))
            #         edges.append(edge)
            # g.add_edges(edges)
            # layout = g.layout("kk")
            # igraph.plot(g, layout=layout)
